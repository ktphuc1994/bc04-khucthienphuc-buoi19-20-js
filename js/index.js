const LOCALSTORAGE_DSNV = "DSNV";
var dsnv = [];
var tempLocalStorageDSNV = localStorage.getItem(LOCALSTORAGE_DSNV);
if (tempLocalStorageDSNV != null) {
  dsnv = JSON.parse(tempLocalStorageDSNV);
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    dsnv[i] = new NhanVien(
      nv.username,
      nv.tenNV,
      nv.email,
      nv.password,
      nv.startDate,
      nv.baseSalary,
      nv.position,
      nv.workTime
    );
  }
  renderDSNV(dsnv);
}
// START BUTTON THÊM
document.getElementById("btnThem").onclick = function () {
  document.getElementById("btnCapNhat").style.display = "none";
};
// END BUTTON THÊM

// START DISMISS FORM
$("#myModal").on("hidden.bs.modal", function (e) {
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "inline-block";
  document.getElementById("tknv").disabled = false;
  xoaThongTinTrenForm();
  xoaThongBaoTrenForm();
  eyeOpened.style.display = "inline-block";
  eyeClosed.style.display = "none";
});
// END DISMISS FORM

// START THÊM NHÂN VIÊN
document.getElementById("btnThemNV").onclick = function () {
  var newNV = layThongTinTuForm();
  // console.log(newNV);
  var isValid = checkUsername(newNV, dsnv);
  isValid &= validateFormWithoutUsername(newNV);
  if (isValid) {
    dsnv.push(newNV);
    localStorage.setItem(LOCALSTORAGE_DSNV, JSON.stringify(dsnv));
    renderDSNV(dsnv);
    xoaThongTinTrenForm();
    eyeOpened.style.display = "inline-block";
    eyeClosed.style.display = "none";
    document.getElementById("status-update").innerText =
      "Thêm nhân viên thành công";
    FadeOut("status-update", 3000);
  }
};
// END THÊM NHÂN VIÊN

// START SỬA THÔNG TIN NHÂN VIÊN
function suaThongTinNV(user) {
  //   console.log("yes");
  var index = dsnv.findIndex(function (nv) {
    return nv.username == user;
  });
  showThongTinNV(dsnv[index]);
  document.getElementById("btnThemNV").style.display = "none";
  document.getElementById("btnCapNhat").style.display = "inline-block";
}
// END SỬA THÔNG TIN NHÂN VIÊN

// START CẬP NHẬT THÔNG TIN NHÂN VIÊN
document.getElementById("btnCapNhat").onclick = function () {
  var updatedNV = layThongTinTuForm();
  var isValid = validateFormWithoutUsername(updatedNV);
  if (isValid) {
    user = updatedNV.username;
    var index = dsnv.findIndex(function (nv) {
      return nv.username == user;
    });
    if (index != -1) {
      dsnv[index] = updatedNV;
      localStorage.setItem(LOCALSTORAGE_DSNV, JSON.stringify(dsnv));
      document
        .getElementById("tableDanhSach")
        .children[index].replaceWith(createNvTR(updatedNV));
      document.getElementById("tknv").disabled = false;
      xoaThongTinTrenForm();
      document.getElementById("btnThemNV").style.display = "inline-block";
      document.getElementById("btnCapNhat").style.display = "none";
      eyeOpened.style.display = "inline-block";
      eyeClosed.style.display = "none";
      document.getElementById("status-update").innerText =
        "Cập nhật thành công";
      FadeOut("status-update", 3000);
    }
  }
};
// START CẬP NHẬT THÔNG TIN NHÂN VIÊN

// START XÓA NHÂN VIÊN
function xoaNV(user) {
  var index = dsnv.findIndex(function (nv) {
    return nv.username == user;
  });
  if (index != -1) {
    dsnv.splice(index, 1);
    localStorage.setItem(LOCALSTORAGE_DSNV, JSON.stringify(dsnv));
    document.getElementById("tableDanhSach").children[index].remove();
  }
}
// END XÓA NHÂN VIÊN

// START ADD ENTER TO SEARCH
var searchInput = document.getElementById("searchName");
searchInput.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault;
    document.getElementById("btnTimNV").click();
  }
});
// END ADD ENTER TO SEARCH

// START TÌM NHÂN VIÊN THEO XẾP LOẠI
document.getElementById("btnTimNV").onclick = function () {
  var typeOfRanking = document.getElementById("searchName").value;
  if (typeOfRanking == "") {
    renderDSNV(dsnv);
  } else {
    var filterDSNV = dsnv.filter(function (nv) {
      return nv.ranking() == typeOfRanking;
    });
    renderDSNV(filterDSNV);
  }
};
// END TÌM NHÂN VIÊN THEO XẾP LOẠI

// START SẮP XẾP TĂNG
document.getElementById("SapXepTang").onclick = function () {
  dsnv.sort(function (a, b) {
    return a.username - b.username;
  });
  renderDSNV(dsnv);
  localStorage.setItem(LOCALSTORAGE_DSNV, JSON.stringify(dsnv));
  document.getElementById("SapXepTang").style.display = "none";
  document.getElementById("SapXepGiam").style.display = "inline-block";
};
// END SẮP XẾP TĂNG

// START SẮP XẾP GIẢM
document.getElementById("SapXepGiam").onclick = function () {
  dsnv.sort(function (a, b) {
    return b.username - a.username;
  });
  renderDSNV(dsnv);
  localStorage.setItem(LOCALSTORAGE_DSNV, JSON.stringify(dsnv));
  document.getElementById("SapXepTang").style.display = "inline-block";
  document.getElementById("SapXepGiam").style.display = "none";
};
// END SẮP XẾP GIẢM
