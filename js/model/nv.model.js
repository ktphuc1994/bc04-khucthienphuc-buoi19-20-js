function NhanVien(
  username,
  tenNV,
  email,
  password,
  startDate,
  baseSalary,
  position,
  workTime
) {
  this.username = username;
  this.tenNV = tenNV;
  this.email = email;
  this.password = password;
  this.startDate = startDate;
  this.baseSalary = baseSalary;
  this.position = position;
  this.workTime = workTime;
  this.totalSalary = function () {
    switch (position) {
      case "Sếp":
        return baseSalary * 3;
      case "Trưởng phòng":
        return baseSalary * 2;
      default:
        return baseSalary * 1;
    }
  };
  this.ranking = function () {
    if (workTime * 1 >= 192) {
      return "Xuất sắc";
    } else if (workTime * 1 >= 176) {
      return "Giỏi";
    } else if (workTime * 1 >= 160) {
      return "Khá";
    } else {
      return "Trung Bình";
    }
  };
}
