const tableDanhSachNV = document.getElementById("tableDanhSach");
// START LẤY THÔNG TIN TỪ FORM
function layThongTinTuForm() {
  var username = document.getElementById("tknv").value;
  var tenNV = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var startDate = document.getElementById("datepicker").value;
  var baseSalary = document.getElementById("luongCB").value;
  var positionTag = document.getElementById("chucvu");
  var position = positionTag.options[positionTag.selectedIndex].text;
  //   console.log("position: ", position);
  var workTime = document.getElementById("gioLam").value;
  return new NhanVien(
    username,
    tenNV,
    email,
    password,
    startDate,
    baseSalary,
    position,
    workTime
  );
}
// END LẤY THÔNG TIN TỪ FORM

// START XÓA THÔNG TIN TRÊN FORM
function xoaThongTinTrenForm() {
  document.getElementById("tknv").value = "";
  document.getElementById("tknv").disabled = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("datepicker").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("chucvu").options[0].selected = true;
  document.getElementById("gioLam").value = "";
}
// END XÓA THÔNG TIN TRÊN FORM

// START XÓA THÔNG BÁO TRÊN FORM
function xoaThongBaoTrenForm() {
  document.getElementById("tbTKNV").style.display = "none";
  document.getElementById("tbTen").style.display = "none";
  document.getElementById("tbEmail").style.display = "none";
  document.getElementById("tbMatKhau").style.display = "none";
  document.getElementById("tbNgay").style.display = "none";
  document.getElementById("tbLuongCB").style.display = "none";
  document.getElementById("tbChucVu").style.display = "none";
  document.getElementById("tbGioLam").style.display = "none";
}
// END XÓA THÔNG BÁO TRÊN FORM

// START TẠO TAG TR CHO MỖI NHÂN VIÊN
function createNvTR(nv) {
  var newNvTr = document.createElement("tr");
  newNvTr.innerHTML = `<td>${nv.username}</td>
  <td>${nv.tenNV}</td>
  <td>${nv.email}</td>
  <td>${nv.startDate}</td>
  <td>${nv.position}</td>
  <td>${nv.totalSalary()}</td>
  <td>${nv.ranking()}</td>
  <td>
  <a class="text-warning mr-1" style="cursor: pointer" data-target="#myModal" data-toggle="modal" onclick="suaThongTinNV('${
    nv.username
  }')"><i class="fa-regular fa-pen-to-square"></i></a>
  <a class="text-danger" style="cursor: pointer"><i class="fa-regular fa-trash-can" onclick="xoaNV('${
    nv.username
  }')"></i></i></button>
  </td>`;
  return newNvTr;
}
// END TẠO TAG TR CHO MỖI NHÂN VIÊN

// START RENDER DANH SÁCH NHÂN VIÊN RA GIAO DIỆN
function renderDSNV(nvArr) {
  tableDanhSachNV.innerHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    tableDanhSachNV.append(createNvTR(nvArr[i]));
  }
}
// END RENDER DANH SÁCH NHÂN VIÊN RA GIAO DIỆN

// START SHOW THÔNG TIN NHÂN VIÊN LÊN FORM
function showThongTinNV(nv) {
  document.getElementById("tknv").value = nv.username;
  document.getElementById("tknv").disabled = true;
  document.getElementById("name").value = nv.tenNV;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.startDate;
  document.getElementById("luongCB").value = nv.baseSalary;
  var chucVuOptions = Array.from(document.getElementById("chucvu").options);
  var selectedChuVuIndex = chucVuOptions.findIndex(function (opt) {
    return opt.text == nv.position;
  });
  document.getElementById("chucvu").options[selectedChuVuIndex].selected = true;
  document.getElementById("gioLam").value = nv.workTime;
}
// END SHOW THÔNG TIN NHÂN VIÊN LÊN FORM

// START USERNAME VALIDATION
function checkUsername(nv, dsnv) {
  var isValid =
    validation.checkEmpty(nv.username, "tbTKNV", "Tài khoản") &&
    validation.checkDuplicate(
      nv.username,
      dsnv,
      "tbTKNV",
      "Tài khoản đã được sử dụng, vui lòng chọn tài khoản khác"
    ) &&
    validation.checkStringLength(
      nv.username,
      "tbTKNV",
      "Tài khoản phải là số, từ 4 đến 6 ký số",
      4,
      6
    ) &&
    validation.checkNumber(nv.username, "tbTKNV", "Tài khoản");
  return isValid;
}
// END USERNAME VALIDATION

// START FORM VALIDATION (WITHOUT USERNAME)
function validateFormWithoutUsername(nv) {
  // START Kiểm tra Tên Nhân Viên
  var isValid =
    validation.checkEmpty(nv.tenNV, "tbTen", "Tên Nhân Viên") &&
    validation.checkName(
      nv.tenNV,
      "tbTen",
      "Tên Nhân Viên không bao gồm số và ký tự đặc biệt"
    );
  // END Kiểm tra Tên Nhân Viên

  // START Kiểm tra Email
  isValid &=
    validation.checkEmpty(nv.email, "tbEmail", "Email") &&
    validation.checkEmail(nv.email, "tbEmail", "Email không đúng định dạng");
  // START Kiểm tra Email

  // START Kiểm tra Mật khẩu
  isValid &=
    validation.checkEmpty(nv.password, "tbMatKhau", "Mật khẩu") &&
    validation.checkPassword(
      nv.password,
      "tbMatKhau",
      "Mật Khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa & 1 ký tự đặc biệt)"
    );
  // END Kiểm tra Mật khẩu

  // START Ngày bắt đầu làm việc
  isValid &=
    validation.checkEmpty(nv.startDate, "tbNgay", "Ngày bắt đầu làm việc") &&
    validation.checkDate(nv.startDate, "tbNgay");
  // END Ngày bắt đầu làm việc

  // START Lương cơ bản
  isValid &=
    validation.checkEmpty(nv.baseSalary, "tbLuongCB", "Lương Cơ Bản") &&
    validation.checkNumber(nv.baseSalary, "tbLuongCB", "Lương Cơ Bản") &&
    validation.checkValue(
      nv.baseSalary * 1,
      "tbLuongCB",
      "Lương Cơ Bản phải nằm trong khoảng từ 1,000,000 đến 20,000,000",
      1000000,
      20000000
    );
  // END Lương cơ bản

  // START Chức vụ
  isValid &= validation.checkPosition(
    nv.position,
    "tbChucVu",
    "Chức vụ phải là: Sếp, Trưởng phòng, hoặc Nhân viên"
  );
  // END Chức vụ

  // START Giờ làm
  isValid &=
    validation.checkEmpty(nv.workTime, "tbGioLam", "Số giờ làm") &&
    validation.checkNumber(nv.workTime, "tbGioLam", "Giờ làm") &&
    validation.checkValue(
      nv.workTime * 1,
      "tbGioLam",
      "Giờ làm phải nằm trong khoảng từ 80 đến 200",
      80,
      200
    );
  // END Giờ làm

  return isValid;
}
// END FORM VALIDATION (WITHOUT USERNAME)

// START FADEOUT ANIMATION
function FadeOut(whereToFade, animateTime) {
  const effects = [{ opacity: 1, offset: 0.75 }, { opacity: 0 }];
  const options = {
    duration: animateTime,
    fill: "both",
  };
  document.getElementById(whereToFade).animate(effects, options);
}
// END FADEOUT ANIMATION
